package fragment;

/**
 * Created by phong on 6/16/2015.
 */

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.SearchView;

import com.example.phong.bai2.AddContact;
import com.example.phong.bai2.DetailContact;
import com.example.phong.bai2.MainActivity;
import com.example.phong.bai2.R;

import java.util.ArrayList;
import java.util.List;

import adapter.Contact;
import adapter.MyAdapter;
import adapter.RecyclerItemClickListener;
import adapter.SimpleDividerItemDecoration;

public class FragmentContact extends Fragment implements
         SearchView.OnQueryTextListener{

    List<Contact> listAdapter = new ArrayList<Contact>();
    //ContactAdapter adapter;

    private RecyclerView mRecyclerView;
    private MyAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    MainActivity main;
    SearchView search;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fetchContacts();
        main = (MainActivity)getActivity();
        //search = (SearchView) getActivity().findViewById(R.id.search);
        //search.setOnQueryTextListener(this);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_contact, container, false);

        main = (MainActivity)getActivity();
        main.search.setOnQueryTextListener(this);


       // cai dat recycle view
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MyAdapter(getActivity(), listAdapter);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getResources()));
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        // do whatever
                        //view.setBackgroundColor(Color.BLUE);


                        Contact rowItem = mAdapter.getItem(position);
                        Intent i = new Intent(getActivity(),DetailContact.class);
                        List<String> test;

                        i.putExtra("id", rowItem.getId());
                        i.putExtra("name",rowItem.getName());
                        i.putExtra("phone",rowItem.getPhone_number());
                        i.putExtra("email",rowItem.getEmail());
                        startActivity(i);
                        getActivity().finish();
                    }
                })
        );


        ImageButton add = (ImageButton)rootView.findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), AddContact.class);
                startActivity(in);
            }
        });


        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        search = (SearchView) getActivity().findViewById(R.id.search);
        search.setOnQueryTextListener(this);
    }

    public void fetchContacts() {



        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;

        String _ID = ContactsContract.Contacts._ID;
        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

        Uri EmailCONTENT_URI =  ContactsContract.CommonDataKinds.Email.CONTENT_URI;
        String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
        String DATA = ContactsContract.CommonDataKinds.Email.DATA;

        StringBuffer output = new StringBuffer();

        ContentResolver contentResolver = getActivity().getContentResolver();

        Cursor cursor = contentResolver.query(CONTENT_URI, null,null, null, null);

        // Loop for every contact in the phone
        if (cursor.getCount() > 0) {

            while (cursor.moveToNext())
            {

                String phoneNumber = "";
                String email = "";
                String contact_id = cursor.getString(cursor.getColumnIndex( _ID ));
                String name = cursor.getString(cursor.getColumnIndex( DISPLAY_NAME ));
                List<String> phonenumberInContact = new ArrayList<String>();

                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex( HAS_PHONE_NUMBER )));

                if (hasPhoneNumber > 0)
                {

                    Log.e("First Name:", name);


                    // Query and loop for every phone number of the contact
                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[] { contact_id }, null);

                    while (phoneCursor.moveToNext())
                    {
                        phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                        //phonenumberInContact.add(phoneNumber);
                        Log.e(" Phone number:", phoneNumber);

                    }

                    phoneCursor.close();
                    //retrieveContactPhoto(contact_id);

                    // Query and loop for every email of the contact
                    Cursor emailCursor = contentResolver.query(EmailCONTENT_URI,	null, EmailCONTACT_ID+ " = ?", new String[] { contact_id }, null);

                    while (emailCursor.moveToNext())
                    {

                        email = emailCursor.getString(emailCursor.getColumnIndex(DATA));


                    }

                    emailCursor.close();
                }
                if(phoneNumber==null)
                {
                    phoneNumber = "";
                }

                Contact con = new Contact(contact_id,name,email,phoneNumber);


                listAdapter.add(con);
            }



        }
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Log.e("search","true");
        mAdapter.getFilter().filter(newText);
        main.mAdapter.getFilter().filter(newText);
         //FragmentFavorite.

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }
}
