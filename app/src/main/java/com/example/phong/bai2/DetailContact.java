package com.example.phong.bai2;

import android.app.AlertDialog;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


public class DetailContact extends ActionBarActivity {
    ImageView photo;
    TextView name;
    TextView phone;
    TextView email;
    ImageView favorite;
    String id_contact;
    String name_contact;
    String phone_contact;
    String email_contact;
    boolean isFavoreite;
    long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_contact);



        //init view
        photo = (ImageView)findViewById(R.id.photo);
        name =(TextView)findViewById(R.id.name);
        phone = (TextView)findViewById(R.id.phone);
        email = (TextView)findViewById(R.id.email);
        favorite = (ImageButton)findViewById(R.id.favorite);

        Bundle bund = getIntent().getExtras();
        if(bund !=null) {

            id_contact = bund.getString("id");
            name_contact = bund.getString("name");
            phone_contact = bund.getString("phone");
            email_contact = bund.getString("email");
            Log.e("phone de",phone_contact);

            //name.setText(name_class);
            //address.setText(add_class);
        }
        if(phone_contact==null || phone_contact.equals(""))
        {
            Log.e("phone",phone_contact);

            phone_contact = "";
            isFavoreite = false;
        }
        else {
            Log.e("phone",phone_contact);
            isFavoreite = fromFavourites(this, phone_contact);
        }
        if(isFavoreite)
        {
            favorite.setImageResource(R.drawable.bookmark);
        }
        else
        {
            favorite.setImageResource(R.drawable.bookmark_non);
        }
        favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phone_contact == null || phone_contact.equals(""))
                {
                    new AlertDialog.Builder(DetailContact.this).setIcon(R.drawable.ic_action_remove).setTitle("Set Favorite")
                            .setMessage("Can not add favorite  this contact because phone number not exist?")
                            .setPositiveButton("Back to main", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {


                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);

                                }
                            }).setNegativeButton("Stay here", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                /*Fragment fragment = new HomeFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, fragment).commit();
                getActivity().setTitle("HOME PAGE");*/
                            dialogInterface.dismiss();
                        }
                    }).show();

                }
                 else {
                    if (fromFavourites(getApplication(), phone_contact)) {
                        unFavorite(id_contact);
                        favorite.setImageResource(R.drawable.bookmark_non);

                    } else {
                        setFavorite(id_contact);
                        favorite.setImageResource(R.drawable.bookmark);
                    }


                }
            }
        });

        retrieveContactPhoto(id_contact);
        name.setText(name_contact);
        phone.setText("Phone number:"+phone_contact);
        if(email_contact!=null)
        {
            email.setText("Email:" + email_contact);
        }


        Button call = (Button)findViewById(R.id.call);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + phone_contact));
                startActivity(callIntent);
            }
        });

        Button add = (Button)findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplication(),SendMessage.class);
                i.putExtra("phone", phone_contact);
                startActivity(i);
                finish();
            }
        });


        //init toolbar

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageButton delete = (ImageButton) findViewById(R.id.delete_contact);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("delete","ok");

                //int i = getContentResolver().delete(Contacts.CONTENT_URI, Contacts.DISPLAY_NAME +"= 'Leo'",null);
                //System.out.println("rows deleted "+i);

                new AlertDialog.Builder(DetailContact.this).setIcon(R.drawable.ic_action_remove).setTitle("Delete")
                        .setMessage("Delete this contact?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                deleteContact(id_contact);
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);

                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                /*Fragment fragment = new HomeFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, fragment).commit();
                getActivity().setTitle("HOME PAGE");*/
                               dialogInterface.dismiss();
                            }
                        }).show();

            }
        });
        ImageButton edit = (ImageButton)findViewById(R.id.edit_contact);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in  = new Intent(getApplication(),EditContact.class);
                in.putExtra("id", id_contact);
                in.putExtra("name",name_contact);
                in.putExtra("phone",phone_contact);
                in.putExtra("email",email_contact);
                startActivity(in);
                finish();
            }
        });


        ImageButton back = (ImageButton)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(getApplication(),MainActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }

    public void setFavorite(String id)
    {
        ContentValues values = new ContentValues();
        String[] fv = new String[] { id };
        values.put(ContactsContract.Contacts.STARRED, 1);
        getContentResolver().update(ContactsContract.Contacts.CONTENT_URI, values,
                ContactsContract.Contacts._ID + "= ?", fv);
    }

    public void unFavorite(String id)
    {
        ContentValues values = new ContentValues();
        String[] fv = new String[] { id };
        values.put(ContactsContract.Contacts.STARRED, 0);
        getContentResolver().update(ContactsContract.Contacts.CONTENT_URI, values,
                ContactsContract.Contacts._ID + "= ?", fv);
    }

    public static boolean fromFavourites(Context context, String phoneNumber) {
        Log.e("phone",phoneNumber);
        if(phoneNumber =="8" )
        {
            return  false;
        }
        else {
            final String[] projection = new String[]{ContactsContract.PhoneLookup.STARRED};
            Uri lookupUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber)); //use this to look up a phone number
            Cursor cursor = context.getContentResolver().query(lookupUri, projection,
                    ContactsContract.PhoneLookup.NUMBER+ "=?",
                    new String[]{phoneNumber}, null);
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    if (cursor.getInt(cursor.getColumnIndex(ContactsContract.PhoneLookup.STARRED)) == 1) {
                        Log.e("OUTPUT: ", "true");
                        return true;
                    }
                    cursor.moveToNext();
                }
            }
        }
        return false;
    }

    private void deleteContact(String nm)
    {
        ContentResolver cr = getContentResolver();
        String where = ContactsContract.Data.CONTACT_ID + " = ? ";
        String[] params = new String[] {nm};

        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        ops.add(ContentProviderOperation.newDelete(ContactsContract.RawContacts.CONTENT_URI)
                .withSelection(where, params)
                .build());
        try {
            cr.applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static boolean deleteContact(Context ctx,String phoneNumber) {
        Uri contactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(phoneNumber));
        Cursor cur = ctx.getContentResolver().query(contactUri, null, null,
                null, null);
        try {
            if (cur.moveToFirst()) {
                do {
                    String lookupKey =
                            cur.getString(cur.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
                    Uri uri = Uri.withAppendedPath(
                            ContactsContract.Contacts.CONTENT_LOOKUP_URI,
                            lookupKey);
                    ctx.getContentResolver().delete(uri, null, null);
                } while (cur.moveToNext());
            }

        } catch (Exception e) {
            System.out.println(e.getStackTrace());
        }
        return false;
    }



    private void retrieveContactPhoto(String contactID) {

        Bitmap photos = null;

        try {
            InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(getContentResolver(),
                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(contactID)));

            if (inputStream != null) {
                photos = BitmapFactory.decodeStream(inputStream);
                //ImageView imageView = (ImageView) findViewById(R.id.img_contact);
                photo.setImageBitmap(photos);

            }
            else
            {
                photos= BitmapFactory.decodeResource(getResources(), R.drawable.noname);
                photo.setImageBitmap(photos);
            }

            if( inputStream != null)
                inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        Intent in = new Intent(this,MainActivity.class);
        startActivity(in);
        //super.onBackPressed();
    }
}
