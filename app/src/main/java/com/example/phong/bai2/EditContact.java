package com.example.phong.bai2;

import android.app.ProgressDialog;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;

import adapter.Const;


public class EditContact extends ActionBarActivity {

    public static String TAG = "error add";
    ImageButton photo;
    ImageView displayphoto;
    String path="";
    Context context=null;
    String photo_url;
    Drawable photo_drawable;

    String id_contact;
    String name_contact;
    String phone_contact;
    String email_contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_edit_contact);

        context= EditContact.this;
        photo_url = resIdToUri(this,R.drawable.ic_launcher).toString();
        Bundle bund = getIntent().getExtras();
        if(bund !=null) {
            id_contact = bund.getString("id");
            name_contact = bund.getString("name");
            phone_contact = bund.getString("phone");
            email_contact = bund.getString("email");


            //name.setText(name_class);
            //address.setText(add_class);
        }



        final EditText editname = (EditText)findViewById(R.id.editname);
        final EditText editphone = (EditText)findViewById(R.id.editphone);
        final EditText editemail = (EditText)findViewById(R.id.editemail);
        editname.setText(name_contact);
        editemail.setText(email_contact);
        editphone.setText(phone_contact);

        photo = (ImageButton)findViewById(R.id.editphoto);
        displayphoto = (ImageView)findViewById(R.id.displayphoto);
        displayphoto.setImageBitmap(retrieveContactPhoto(id_contact));
        registerForContextMenu(photo);
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("open", "ok");
                openContextMenu(v);
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageButton s = (ImageButton) findViewById(R.id.accept);
        s.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("delete","ok");
                String name = editname.getText().toString();
                String phone = editphone.getText().toString();
                String email = editemail.getText().toString();
                update(id_contact,name,phone,photo_url,email);
                Intent in = new Intent(getApplication(),MainActivity.class);
                finish();
                startActivity(in);

            }
        });
    }

    public static Uri resIdToUri(Context context, int resId) {
        return Uri.parse(Const.ANDROID_RESOURCE + context.getPackageName()
                + Const.FORESLASH + resId);
    }


    private Bitmap retrieveContactPhoto(String contactID) {

        Bitmap photos = null;

        try {
            InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(context.getContentResolver(),
                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(contactID)));

            if (inputStream != null) {
                photos = BitmapFactory.decodeStream(inputStream);

                //imageView.setImageBitmap(photos);

            } else {
                photos = BitmapFactory.decodeResource(context.getResources(), R.drawable.noname);
            }

            if (inputStream != null)
                inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return photos;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        super.onCreateContextMenu(menu, v, menuInfo);

        menu.setHeaderTitle("Post Image");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.camer_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.take_photo:
                //Toast.makeText(context, "Selected Take Photo", Toast.LENGTH_SHORT).show();
                takePhoto();
                break;

            case R.id.choose_gallery:
                //Toast.makeText(context, "Selected Gallery", Toast.LENGTH_SHORT).show();
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, 1);

                break;

            case R.id.share_cancel:
                closeContextMenu();
                break;
            default:
                return super.onContextItemSelected(item);
        }
        return true;
    }

    public void takePhoto()
    {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        File folder = new File(Environment.getExternalStorageDirectory() + "/LoadImg");

        if(!folder.exists())
        {
            folder.mkdir();
        }
        final Calendar c = Calendar.getInstance();
        String new_Date= c.get(Calendar.DAY_OF_MONTH)+"-"+((c.get(Calendar.MONTH))+1)   +"-"+c.get(Calendar.YEAR) +" " + c.get(Calendar.HOUR) + "-" + c.get(Calendar.MINUTE)+ "-"+ c.get(Calendar.SECOND);
        path=String.format(Environment.getExternalStorageDirectory() +"/LoadImg/%s.png","LoadImg("+new_Date+")");
        File photo = new File(path);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
        startActivityForResult(intent, 2);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==1)
        {
            if(data!=null) {
                Uri photoUri = data.getData();
                if (photoUri != null) {
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(photoUri, filePathColumn, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String filePath = cursor.getString(columnIndex);
                    cursor.close();
                    Log.v("Load Image", "Gallery File Path=====>>>" + filePath);
                    photo_url = filePath;
                    //Log.v("Load Image", "Image List Size=====>>>"+image_list.size());

                    //updateImageTable();
                    new GetImages().execute();
                }
            }
        }

        if(requestCode==2)
        {
            Log.v("Load Image", "Camera File Path=====>>>"+path);
            photo_url = path;
            //Log.v("Load Image", "Image List Size=====>>>"+image_list.size());
           // updateImageTable();
            new GetImages().execute();

        }
    }

    public void updateImageTable()
    {


        displayphoto.setImageDrawable(photo_drawable);

    }

    public Drawable loadImagefromurl(Bitmap icon)
    {
        Drawable d=new BitmapDrawable(icon);
        return d;
    }

    public class GetImages extends AsyncTask<Void, Void, Void>
    {
        public ProgressDialog progDialog=null;

        protected void onPreExecute()
        {
            progDialog=ProgressDialog.show(context, "", "Loading...",true);
        }
        @Override
        protected Void doInBackground(Void... params)
        {

            Bitmap bitmap = BitmapFactory.decodeFile(photo_url.toString().trim());
            bitmap = Bitmap.createScaledBitmap(bitmap,500, 500, true);
            Drawable d=loadImagefromurl(bitmap);
            photo_drawable = d;


            return null;
        }

        protected void onPostExecute(Void result)
        {
            if(progDialog.isShowing())
            {
                progDialog.dismiss();
            }
            updateImageTable();
        }
    }

    public void update(String id,String firstname,String number,String photo_uri,String email)
    {


        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

        // Name
        ContentProviderOperation.Builder builder = ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI);
        builder.withSelection(ContactsContract.Data.CONTACT_ID + "=?" +
                " AND " + ContactsContract.Data.MIMETYPE + "=?",
                new String[]{String.valueOf(id), ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE});
        builder.withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, firstname);
        builder.withValue(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME, "");
        ops.add(builder.build());

        // Number
        builder = ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI);
        builder.withSelection(ContactsContract.Data.CONTACT_ID + "=?" +
                " AND " + ContactsContract.Data.MIMETYPE + "=?",
                new String[]{String.valueOf(id), ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE});
        builder.withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, number);
        ops.add(builder.build());

        //Email

        builder = ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI);
        builder.withSelection(ContactsContract.Data.CONTACT_ID + "=?" +
                        " AND " + ContactsContract.Data.MIMETYPE + "=?",
                new String[]{String.valueOf(id), ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE});
        builder.withValue(ContactsContract.CommonDataKinds.Email.ADDRESS, email);
        ops.add(builder.build());



        // Picture

            try {

                //BitmapFactory.Options imageOpts = new BitmapFactory.Options ();
                //imageOpts.inSampleSize = 1;   // for 1/2 the image to be loaded
                //Bitmap bitmap = Bitmap.createScaledBitmap (BitmapFactory.decodeFile(photo_uri, imageOpts), 70, 70, false);
              //  Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.parse(photo_uri));
                Bitmap bitmap = BitmapFactory.decodeFile(photo_uri);
                ByteArrayOutputStream image = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, image);




                builder = ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI);
                builder.withSelection(ContactsContract.Data.CONTACT_ID + "=?" + " AND " + ContactsContract.Data.MIMETYPE + "=?", new String[]{String.valueOf(id), ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE});
                builder.withValue(ContactsContract.CommonDataKinds.Photo.PHOTO, image.toByteArray());
                ops.add(builder.build());
            } catch (Exception e) {
                e.printStackTrace();
            }


        // Update
        try
        {
            getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void Insert2Contacts(Context ctx, String nameSurname,
                                       String telephone,String imagePath) {
        if (!isTheNumberExistsinContacts(ctx, telephone)) {
            ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
            int rawContactInsertIndex = ops.size();

            ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null).build());
            ops.add(ContentProviderOperation
                    .newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(
                            ContactsContract.Data.RAW_CONTACT_ID,
                            rawContactInsertIndex)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, telephone).build());
            ops.add(ContentProviderOperation
                    .newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.RawContacts.Data.RAW_CONTACT_ID,
                            rawContactInsertIndex)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, nameSurname)
                    .build());






            Bitmap bmImage = BitmapFactory.decodeFile(imagePath);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bmImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] b = baos.toByteArray();



            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Photo.DATA15,b)
                    .build());


            try {
                ContentProviderResult[] res = ctx.getContentResolver()
                        .applyBatch(ContactsContract.AUTHORITY, ops);
            } catch (Exception e) {

                Log.d(TAG, e.getMessage());
            }
        }
    }


    public static boolean isTheNumberExistsinContacts(Context ctx,
                                                      String phoneNumber) {
        Cursor cur = null;
        ContentResolver cr = null;

        try {
            cr = ctx.getContentResolver();

        } catch (Exception ex) {
            Log.d("TAG", ex.getMessage());
        }

        try {
            cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null,
                    null, null);
        } catch (Exception ex) {
            Log.i(TAG, ex.getMessage());
        }

        try {
            if (cur.getCount() > 0) {
                while (cur.moveToNext()) {
                    String id = cur.getString(cur
                            .getColumnIndex(ContactsContract.Contacts._ID));
                    String name = cur
                            .getString(cur
                                    .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    // Log.i("Names", name);
                    if (Integer
                            .parseInt(cur.getString(cur
                                    .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                        // Query phone here. Covered next
                        Cursor phones = ctx
                                .getContentResolver()
                                .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                        null,
                                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                                + " = " + id, null, null);
                        while (phones.moveToNext()) {
                            String phoneNumberX = phones
                                    .getString(phones
                                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            // Log.i("Number", phoneNumber);

                            phoneNumberX = phoneNumberX.replace(" ", "");
                            phoneNumberX = phoneNumberX.replace("(", "");
                            phoneNumberX = phoneNumberX.replace(")", "");
                            if (phoneNumberX.contains(phoneNumber)) {
                                phones.close();
                                return true;

                            }

                        }
                        phones.close();
                    }

                }
            }
        } catch (Exception ex) {
            Log.i(TAG, ex.getMessage());

        }

        return false;
    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/
}