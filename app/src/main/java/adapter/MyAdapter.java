package adapter;

/**
 * Created by phong on 6/12/2015.
 */

import android.content.ContentUris;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.phong.bai2.R;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by phong on 6/10/2015.
 */


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> implements Filterable {
    Context context;
    List<Contact> rowItems;
    List<Contact>mStringFilterList;
    private ValueFilter valueFilter;

    ImageView imageView;

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView name;
        public TextView email;
        public ImageView photo;

        public ViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.name);
            email = (TextView) v.findViewById(R.id.email);
            photo = (ImageView)v.findViewById(R.id.photo);
        }
    }

    public MyAdapter(Context context, List<Contact> items) {
        this.context = context;
        this.rowItems = items;
        mStringFilterList = items;
    }

    /*private view holder class*/
    /*private class ViewHolder {
        ImageView imageView;
        TextView txtTitle;
        TextView txtDesc;
    }*/
    private Drawable retrieveContactPhoto(String contactID) {

        Bitmap photos = null;
        Drawable d= null;

        try {
            InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(context.getContentResolver(),
                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(contactID)));

            if (inputStream != null) {
                photos = BitmapFactory.decodeStream(inputStream);
                photos = Bitmap.createScaledBitmap(photos,500, 500, true);
                d=loadImagefromurl(photos);
                //photo_drawable = d;

                //imageView.setImageBitmap(photos);

            }
            else
            {
                 photos= BitmapFactory.decodeResource(context.getResources(), R.drawable.noname);
                photos = Bitmap.createScaledBitmap(photos,500, 500, true);
                 d=loadImagefromurl(photos);
            }

            if( inputStream != null)
                inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return d;

    }

    public Drawable loadImagefromurl(Bitmap icon)
    {
        Drawable d=new BitmapDrawable(icon);
        return d;
    }

    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_contact, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Contact contact = rowItems.get(position);
        holder.name.setText(contact.getName());
        holder.email.setText(contact.getPhone_number());
        //holder.photo.setImageBitmap(retrieveContactPhoto(String.valueOf(contact.getId())));
        holder.photo.setImageDrawable(retrieveContactPhoto(String.valueOf(contact.getId())));



    }

    public Contact getItem(int position) {
        return rowItems.get(position);
    }
    @Override
    public int getItemCount() {
        return rowItems.size();
    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                List<Contact> filterList = new ArrayList<Contact>();
                for (int i = 0; i < mStringFilterList.size(); i++) {
                    if ( (mStringFilterList.get(i).getName().toUpperCase() )
                            .contains(constraint.toString().toUpperCase())) {

                        Contact country = new Contact(mStringFilterList.get(i)
                                .getId() ,  mStringFilterList.get(i)
                                .getName() ,  mStringFilterList.get(i)
                                .getEmail(),mStringFilterList.get(i).getPhone_number());

                        filterList.add(country);
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = mStringFilterList.size();
                results.values = mStringFilterList;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            rowItems = (ArrayList<Contact>) results.values;
            notifyDataSetChanged();
        }

    }


}
